using System;
using Testik.DAL.Entities;
using System.Collections.Generic;

namespace Testik.DAL.Entityes
{
    public class TestikEntity : DomainObject
    {
        public string Description { get; set; }
        
        public DateTime CreateDate { get; set; }
        
        public int Count { get; set; }
    }
}