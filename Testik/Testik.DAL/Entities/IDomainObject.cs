namespace Testik.DAL.Entities
{
    public interface IDomainObject
    {
        int Id { get; set; }
    }
}