using System.ComponentModel.DataAnnotations;

namespace Testik.DAL.Entities
{
    public abstract class DomainObject: IDomainObject
    {
        [Key]
        public int Id { get; set; }
    }
}