using System.Threading.Tasks;

namespace Testik.DAL
{
    
    public interface IProvider
    {
        TestikDbContext DbContext { get; }

        Task SaveChangesAsync();
        
        void SaveChange();
    }
}