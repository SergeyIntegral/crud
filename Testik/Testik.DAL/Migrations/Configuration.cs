using System;
using System.Data.Entity.Migrations;
using Testik.DAL.Entityes;

namespace Testik.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Testik.DAL.TestikDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }



        protected override void Seed(Testik.DAL.TestikDbContext context)
        {
            context.TestikEntities.AddOrUpdate(new TestikEntity()
            {
                Count = 2,
                CreateDate = DateTime.Now,
                Description = "Test1"
            });
            
            
            context.TestikEntities.AddOrUpdate(new TestikEntity()
            {
                Count = 1,
                CreateDate = DateTime.Now.AddDays(-10),
                Description = "Test2"
            });
            
            
            context.TestikEntities.AddOrUpdate(new TestikEntity()
            {
                Count = 24,
                CreateDate = DateTime.Now.AddDays(-34),
                Description = "Test3"
            });
            
            
            
            context.TestikEntities.AddOrUpdate(new TestikEntity()
            {
                Count = 12,
                CreateDate = DateTime.Now.AddDays(-12),
                Description = "Test4"
            });
            
            
        }
    }
}