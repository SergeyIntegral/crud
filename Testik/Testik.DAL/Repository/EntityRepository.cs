using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Testik.DAL.Entities;

namespace Testik.DAL.Repository
{
    public class EntityRepository<TEntity> 
        : IEntityRepository<TEntity> where TEntity : class, IDomainObject
    {

        private readonly IProvider _provider;

        protected TestikDbContext dbContext
        {
            get{return _provider.DbContext;}
        }

        protected DbSet<TEntity> Set
        {
            get { return _provider.DbContext.Set<TEntity>(); }
        }

        public EntityRepository(IProvider provider)
        {
            _provider = provider;
        }
        
        
        
        public virtual TEntity Insert(TEntity entity)
        {
            return Set.Add(entity);
        }

        public virtual TEntity Update(TEntity entity)
        {
            AttachIfNot(entity);
            dbContext.Entry(entity).State = EntityState.Modified;

            return entity;
        }

        public virtual void Delete(int id)
        {
            var entity = Set.Find(id);
            if (entity == null)
                return;
            
            Delete(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            AttachIfNot(entity);
            Set.Remove(entity);
        }

        public virtual TEntity Find(int id)
        {
            return Set.Find(id);
        }

        public virtual async Task<TEntity> FindAsync(int id)
        {
            return await Set.FindAsync(id);
        }

        public virtual IQueryable<TEntity> AsQueryable()
        {
            return Set.AsQueryable();
        }
        
        private void AttachIfNot(TEntity entity)
        {
            if (!Set.Local.Contains(entity))
            {
                Set.Attach(entity);
            }
        }
    }
}