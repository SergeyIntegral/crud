using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Testik.DAL.Entityes;

namespace Testik.DAL
{
    public class TestikDbContext : DbContext
    {
        public TestikDbContext() : base("ConnectTestik")
        {
            
         
        }
        
        public DbSet<TestikEntity> TestikEntities { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }
        
        
        public static TestikDbContext Create()
        {
            return new TestikDbContext();
        }
    }
}