using System.Threading.Tasks;

namespace Testik.DAL
{
    //���� �� ����
    public class Provider : IProvider
    {
        
        private readonly TestikDbContext _dbContext = new TestikDbContext();

        public TestikDbContext DbContext
        {
            get { return _dbContext; }
        }


        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void SaveChange()
        {
            _dbContext.SaveChanges();

        }
    }
}