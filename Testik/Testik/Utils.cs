﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration;
using Autofac.Integration.Mvc;
using Testik.DAL;
using Testik.DAL.Repository;
using Testik.Service;

namespace Testik
{
    /// <summary>
    /// настройка зависимостей для автофага
    /// </summary>
    public static class Utils
    {

        

        private static void RegisterContext(ContainerBuilder builder)
        {
            builder.RegisterType<TestikDbContext>().AsSelf().InstancePerRequest();
            builder.RegisterType<Provider>().As<IProvider>().InstancePerRequest();
        }

        public static void CreateContainer()
        {
            var builder = new ContainerBuilder();

            // DataBase
            RegisterContext(builder);
            
            // Types
            RegisterTypes(builder);

            //MVC
            RegisterInfrastructure(builder);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            
        }

        

        private static void RegisterTypes(ContainerBuilder builder)
        {

            builder.RegisterGeneric(typeof(EntityRepository<>))
                .As(typeof(IEntityRepository<>))
                .InstancePerRequest();


            builder.RegisterGeneric(typeof(EntityService<>))
                .As(typeof(IEntityService<>))
                .InstancePerRequest();

            
        }


        private static void RegisterInfrastructure(ContainerBuilder builder)
        {
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterSource(new ViewRegistrationSource());
            builder.RegisterFilterProvider();
        }


    }
}