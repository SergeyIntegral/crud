﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Testik.DAL;
using Testik.DAL.Entityes;
using Testik.Service;

namespace Testik.Controllers
{
    public class StartController : Controller
    {
        private readonly IProvider _provider;

        private readonly IEntityService<TestikEntity> _service;

        public StartController(IProvider provider, IEntityService<TestikEntity> service)
        {
            this._provider = provider;
            this._service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            return Json(_service.AsQueryable().ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(TestikEntity ent)
        {
            return Json(_service.InsertAndSave(ent), JsonRequestBehavior.AllowGet);
        }
        //не нужна
        //public JsonResult GetbyId(int id)
        //{
        //    var tt = _service.AsQueryable().First(x => x.Id.Equals(id));
        //    return Json(tt, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult Update(TestikEntity ent)
        {
            _service.UpdateAndSave(ent);
            return Json( JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            _service.Delete(id);
            _provider.SaveChange();
            return Json(JsonRequestBehavior.AllowGet);
        }

    }
}