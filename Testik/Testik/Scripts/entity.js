﻿

loadData();




function toDateFromJson(src) {

    var result = "";
    var dt = new Date(parseInt(src.substr(6)));
    result += dt.format("yyyy-mm-dd");
    return result;
}




//Load Data 
function loadData() {

    $.ajax({
        url: "/Start/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            var html = '';
            $.each(result, function (key, item) {

                html += '<tr>';
                html += '<td><text id="Ids">' + item.Id + '</text></td>';
                html += '<td><text> <span class = "display-mode"> ' + item.Description + '</span></text> <input type="text" id="Description" value="' + item.Description + '" class ="edit-mode" /></td>';
                html += '<td><text> <span class = "display-mode"> ' + toDateFromJson(item.CreateDate) + '</span></text><input type="date" id="CreateDate" value="' + toDateFromJson(item.CreateDate) + '" class ="edit-mode" /></td>';
                html += '<td><text> <span class = "display-mode"> ' + item.Count + '</span></text><input type="text" id="Count" value="' + item.Count + '" class ="edit-mode" /></td>';
                html += '<td><button class="edit-user display-mode" onclick="Update(this)" >ИЗМЕНИТЬ</button> <button class="delete-user display-mode" onclick="Delele(' + item.Id + ')">УДАЛИТЬ</button> <button class="cancel-user edit-mode" onclick="Cancel(this)">ОТМЕНА</button> <button class="save-user edit-mode">СОХРАНИТЬ</button>   </td>';
                html += '</tr>';

            });
            $('.tbody').html(html);
            $('.edit-mode').hide();
            //$('.save-mode').hide();
            $('.display-mode').show();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//Add Data 
function Add() {

    $('.Add').hide();

    var tr = '';
    tr += '<tr>';
    tr += '<td><text id="IdInsert"></text></td>';
    tr += '<td><input type="text" id="DescriptionInsert" value="" class ="edit-mode" /></td>';
    tr += '<td><input type="date" id="CreateDateInsert" value="" class ="edit-mode" /></td>';
    tr += '<td><input type="text" id="CountInsert" value="" class ="edit-mode" /></td>';
    tr +=
        '<td><button class="cancel-user edit-mode">ОТМЕНА</button> <button class="save-user edit-mode">СОХРАНИТЬ</button> </td>';
    tr += '</tr>';

    $('.tbody').append(tr);

    $('.cancel-user').on('click',
        function() {
            $(this).parents("tr").remove();
            $('.Add').show();

        });
    $(".save-user").on("click",

        function () {
            var Object = {
                Description: $('#DescriptionInsert').val(),
                CreateDate: $('#CreateDateInsert').val(),
                Count: $('#CountInsert').val()
            };

            // я хз как валидировать дату тут 
            if ($('#CreateDateInsert').val().trim() == "") {

                Object.CreateDate = new Date();
                //console.log(Object.CreateDate);

            }
            


            if (!$.isNumeric($('#CountInsert').val())) {
                alert("Поле Count число, братиш");
            } else {
                
           


                $.ajax({
                    url: "/Start/Add",
                    data: JSON.stringify(Object),
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function(result) {
                        loadData();
                        $('.Add').show();
                    },
                    error: function(errormessage) {
                        alert(errormessage.responseText);
                        $('.Add').show();
                    }
                });
            }
        });
}


function Update(em) {

    var tr = $(em).parents('tr:first');
    tr.find('.edit-mode, .display-mode').toggle();

    $('.save-user').on('click',
        function () {

            var ID = tr.find("#Ids").html();
            var Description = tr.find("#Description").val();
            var CreateDate = tr.find("#CreateDate").val();
            var Count = tr.find("#Count").val();

            var Object = {
                "Id": ID,
                "Description": Description,
                "CreateDate": CreateDate,
                "Count": Count
            };

            if (!$.isNumeric(Count)) {
                alert("Поле Count число, братиш");
            }  else {

                $.ajax({
                    url: "/Start/Update",
                    data: JSON.stringify(Object),
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function(result) {
                        loadData();
                    },
                    error: function(errormessage) {
                        alert(errormessage.responseText);
                    }
                });
            }

        });
}

function Cancel(aa) {
    var tr = $(aa).parents('tr:first');
    tr.find('.edit-mode, .display-mode').toggle();

};

function Delele(id) {
    var ans = confirm("Ты Уверен браток?");
    if (ans) {
        $.ajax({
            url: "/Start/Delete/" + id,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                loadData();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }

        });

    }
};

