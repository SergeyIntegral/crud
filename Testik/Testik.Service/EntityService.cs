using System.Linq;
using System.Threading.Tasks;
using Testik.DAL;
using Testik.DAL.Entities;
using Testik.DAL.Repository;

namespace Testik.Service
{
    public class EntityService<TEntity> : IEntityService<TEntity> where TEntity : class, IDomainObject
    {
        protected IProvider Provider;

        protected IEntityRepository<TEntity> Repository;

        public EntityService(IProvider provider, IEntityRepository<TEntity> repository)
        {
            this.Provider = provider;
            this.Repository = repository;
        }

        public virtual TEntity Insert(TEntity entity)
        {
            return Repository.Insert(entity);
        }

        public virtual TEntity InsertAndSave(TEntity entity)
        {
            var result = Repository.Insert(entity);
            Provider.SaveChange();
            return result;
        }

        public virtual async Task<TEntity> InsertAndSaveAsync(TEntity entity)
        {
            var result = Repository.Insert(entity);
            await Provider.SaveChangesAsync();
            return result;
        }

        public virtual TEntity Update(TEntity entity)
        {
            return Repository.Update(entity);
        }

        public virtual TEntity UpdateAndSave(TEntity entity)
        {
            var result = Repository.Update(entity);
            Provider.SaveChange();
            return result;
        }

        public virtual async Task<TEntity> UpdateAndSaveAsync(TEntity entity)
        {
            var result = Repository.Update(entity);
            await Provider.SaveChangesAsync();
            return result;
        }

        public virtual void Delete(int id)
        {
            Repository.Delete(id);
        }

        public virtual void DeleteAndSave(int id)
        {
            Repository.Delete(id);
            Provider.SaveChange();
        }

        public virtual async Task DeleteAndSaveAsync(int id)
        {
            Repository.Delete(id);
            await Provider.SaveChangesAsync();
        }

        public virtual void Delete(TEntity entity)
        {
            Repository.Delete(entity);
        }

        public virtual void DeleteAndSave(TEntity entity)
        {
            Repository.Delete(entity);
            Provider.SaveChange();
        }

        public virtual async Task DeleteAndSaveAsync(TEntity entity)
        {
            Repository.Delete(entity);
            await Provider.SaveChangesAsync();
        }

        public virtual TEntity Find(int id)
        {
            return Repository.Find(id);
        }

        public virtual async Task<TEntity> FindAsync(int id)
        {
            return await Repository.FindAsync(id);
        }

        public virtual IQueryable<TEntity> AsQueryable()
        {
            return Repository.AsQueryable();
        }
    }
}